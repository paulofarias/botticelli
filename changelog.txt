Botticelli Changelog
===============

Version 0.1.0 - 30.01.2023
--------------------------

* Initial version.



/******************************************************************************
 *
 * Copyright (c) 2023 Paulo Farias <paulofarias7@proton.com>
 * 
 * This file is part of botticelli. 
 * 
 * Botticelli is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Botticelli is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * botticelli. If not, see http://www.gnu.org/licenses/.
 *
 *******************************************************************************/
