#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 10:53:47 2023

@author: paulo
"""

from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import tkinter as tk
from PIL import Image,ImageTk
import os


janela = Tk()
janela.title('Botticelli')
janela.geometry('1200x700')
#janela.configure(bg=co1)
janela.resizable(width=TRUE, height=TRUE)


def showimage():
    filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Selecione uma imagem", filetypes=(("JPG File", "*.jpg"), ("PNG File", "*.png"),("All Files","*.*")))
    img = Image.open(filename)
    img = ImageTk.PhotoImage(img)
    lbl.configure(image=img)
    lbl.image=img



def botticelli():
    messagebox.showinfo(title="Sobre",
                        message= "Botticelli é um visualizador de imagem.\n\
Ele exibe imagens em amplos formatos incluindo PNG,JPEG.\n\
Versão: 0.1.0.\n\
Author: Paulo Farias.\nLicença: GPL3")
                      




frame_logo = Frame(janela, width=350, height=60)
frame_logo.grid(row=0, column=0, pady=1, padx=0, sticky=NSEW)


btn = Button(frame_logo, text='Abrir imagem', fg='orange', command=showimage)
btn.grid(row=0, column=1,)


btn2 = Button(frame_logo, text='botticelli', fg='orange', command=botticelli)
btn2.grid(row=0, column=2,)


frame_corpo = Frame(janela, width=1200, height=700,)
frame_corpo.grid(row=1, column=0, pady=1, padx=0, sticky=NSEW)

lbl = Label(frame_corpo)
lbl.pack()



janela.mainloop()
